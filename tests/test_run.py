# encoding: utf-8
import sys
import os

sys.path.append("..")
from tree import run
from os import listdir


def test():
    treefiles = listdir(".")
    for i in treefiles:
        if ".ctree" in i:
            run(os.path.join(".", i))


test()
