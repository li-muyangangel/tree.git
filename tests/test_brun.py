# encoding: utf-8
import sys
import os

sys.path.append("..")
from tree import brun
from os import listdir


def test():
    treefiles = listdir(".")
    for i in treefiles:
        if "tree" in i:
            brun(os.path.join(".", i))


test()
