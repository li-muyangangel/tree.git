# encoding: utf-8
import sys
import os

sys.path.append("..")
from tree import Dis
from os import listdir


def test():
    treefiles = listdir(".")
    for i in treefiles:
        if ".ctree" in i:
            Dis(os.path.join(".", i))


test()
