# encoding: utf-8
import sys
import os

sys.path.append("..")
from tree import Grammar
from os import listdir


def test():
    treefiles = listdir(".")
    for i in treefiles:
        if ".tree" in i:
            Grammar(os.path.join(".", i))


test()
