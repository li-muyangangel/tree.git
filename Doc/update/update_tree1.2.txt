Tree1.2基于Tree1.1做了以下更新：


1.新增异常表
2.新增了三条命令行指令：
tree brun ***.tree
tree crun ***.tree
tree clean dir
3.新增指令PRINTLN,INPUT,LEN_VALUE,RUN_FUNC;对应函数：input(),println(),len()
4.规范use.txt
5.建立单元测试
6.新增while语句
7.新增Doc\cmd_instruction.txt
8.新增ModuleNotFoundError
9.把指令tree p ***.ctree更名为tree dis ***.ctree
10.类型检测提前到编译环节，提升运行速度
11.支持加减乘除，并修复加减乘除漏洞
12.ctree不再兼容Tree1.1

注：该版本为一个不稳定的系统版本
