# encoding: utf-8
"""
编译器与解释器共同需要的数据存放在此模块中
"""
from Error.Error import send_error
from TVM.types.array import array
from TVM.types.string import string
from TVM.types.null import null


# 版本号
ver = 1.7

# 类型强制转换
types = {"int": int, "string": string, "float": float, "bool": bool, "array": array}

# 字符分割表
english: tuple = (
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "j",
    "k",
    "l",
    "y",
    "z",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "_",
)
# 数字分割表
nums = ("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".", "-")


def what_type(value: str) -> tuple:
    """
    把字符串通过数据的相应特征转化为相应数据和数据类型的形式返回
    用处：":="
    @param value: 字符串数据
    @return: (数据, 数据类型)
    """
    if ((value[0] == '"') and (value[0] == '"')) or (
        (value[-1] == "'") and (value[-1] == "'")
    ):
        return string(value[1:-1]), "string"
    if value[0] in english:
        return string(value), "string"

    if value[0] in nums:
        if "." in value:
            try:
                return float(value), "float"
            except ValueError:
                send_error("ValueError", value, "float")
        try:
            return int(value), "int"
        except ValueError:
            send_error("ValueError", value, "int")

    send_error("ValueError", value, "type")
