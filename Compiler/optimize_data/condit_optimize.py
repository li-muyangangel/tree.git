"""
本模块负责grammar的代码优化————条件表达式
"""

__equal = lambda a, b: a == b
__unequal = lambda a, b: a != b
__greater = lambda a, b: a > b
__less = lambda a, b: a < b
__less_equal = lambda a, b: a <= b
__greater_equal = lambda a, b: a >= b


cond_func_dict = {
    "==": __equal,
    "!=": __unequal,
    "<": __less,
    ">": __greater,
    ">=": __greater_equal,
    "<=": __less_equal,
}
