"""
本模块负责grammar的代码优化————常量折叠
"""
try:
    from Error.Error import send_error
except ImportError:
    from ...Error.Error import send_error


def __div(a, b):
    try:
        return a / b
    except ZeroDivisionError:
        send_error("ZeroDivError", a)


def __zdiv(a, b):
    try:
        return a // b
    except ZeroDivisionError:
        send_error("ZeroDivError", a)


def __zmod(a, b):
    try:
        return a % b
    except ZeroDivisionError:
        send_error("ZeroDivError", a)


__add = lambda a, b: a + b
__sub = lambda a, b: a - b
__mul = lambda a, b: a * b
__pow = lambda a, b: a ** b


op_func_dict = {
    "+": __add,
    "-": __sub,
    "*": __mul,
    "/": __div,
    "**": __pow,
    "//": __zdiv,
    "%": __zmod,
}
