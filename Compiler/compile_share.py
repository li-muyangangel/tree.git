# encoding: utf-8
"""
本模块存放token与grammar共同需要的部分（常量）
"""


# 字符串标识
string = ('"', "'")
# 关键字
keywords = ("if", "while", "for", "func", "class")
# 语句
sentences = ("import", "goto", "del", "assert")
# 运算符
opers = (
    "+",
    "-",
    "**",
    "*",
    "/",
    "//",
    "%",
    "+=",
    "-=",
    "*=",
    "/=",
    "**=",
    "%=",
    "//=",
)
# 逻辑运算符
condits = ("<", ">", "<=", ">=", "!=", "==", "and", "or", "not")
# 赋值符号
aslist = (":=", "=")
# 转义字符字典
change_varchar = {"n": "\n", "t": "\t", "'": "'", '"': '"', "\\": "\\"}
