# encoding: utf-8
"""
词法分析，负责把关键字划分为一个列表返回给compiler
"""
from .compile_share import *

from share import english, nums


# 符号标识
symbols = ("(", ")", "[", "]", "{", "}", "#", ",", ".")
# 所有标识的总和
all_oper = keywords + sentences + opers + string + condits + aslist + symbols
english_nums = english + nums


def inside_token():
    """
    如果检测到字符为关键字，读取它的后一位与前一位相加，判断是否仍然属于关键字，
    是则返回后者，否则返回前者
    """
    global for_index, code_str
    if code_str in all_oper:
        try:
            try_code_str = code_str + i[for_index + 1]
        except IndexError:
            try_code_str = code_str
        if try_code_str in all_oper:
            add_list.append(try_code_str)
            for_index += 1
            code_str = ""
        else:
            add_list.append(code_str)
            code_str = ""
    elif code_str == " ":
        code_str = ""


def token(codes: list) -> list:
    """
    原理：通过一个个字符累计进入相应的划分
    @param codes: 通过Compiler划分处理过的列表
    @return: 被本函数处理过的列表
    """
    # TODO:标签
    result = []
    global add_list, i
    for i in codes:
        # 要加入result的列表
        add_list = []
        global for_index, code_str
        # 未开始累计的原始字符
        code_str = ""
        # 控制读取字符的变量
        for_index = -1
        while 1:
            for_index += 1
            if not for_index <= len(i) - 1:
                # 字符超出边界范围则跳出
                break
            code_str += i[for_index]
            if code_str[0] in string:
                # 字符串
                if (code_str[-1] == code_str[0]) and (len(code_str) >= 3):
                    # 读取到字符串的终点且不为第一个字符时视为字符串结束
                    if isinstance(code_str, str) and "\\" in code_str:
                        final = ""
                        index = 0
                        for char in range(len(code_str)):
                            try:
                                if code_str[index] == "\\":
                                    final += change_varchar[code_str[index + 1]]
                                    index += 2
                                else:
                                    final += code_str[index]
                                    index += 1
                            except IndexError:
                                break
                    else:
                        final = code_str

                    add_list.append(final)
                    # 重置
                    code_str = ""

            elif code_str in all_oper:
                # 当累计字符串等于某个标识时
                if not (
                    code_str == "-"
                    and (
                        (for_index > 0 and i[for_index - 1] == "(")
                        or for_index == 0
                        or (for_index > 0 and (i[for_index - 1] not in nums))
                    )
                ):
                    try:
                        try_code_str = code_str + i[for_index + 1]
                    except IndexError:
                        try_code_str = code_str
                    if try_code_str in all_oper:
                        add_list.append(try_code_str)
                        for_index += 1
                        code_str = ""
                    else:
                        add_list.append(code_str)
                        code_str = ""

            elif code_str == " ":
                # 空格直接略过
                code_str = ""

            elif code_str[0] in english_nums:
                # 变量或者整数，小数
                if code_str[-1] not in english_nums:
                    add_list.append(code_str[:-1])
                    code_str = code_str[-1]
                    inside_token()
        # 将最后一个符号加入列表
        add_list.append(code_str)
        # 每条语句占一个列表
        result.append([i for i in add_list if i != ""])
    return result
