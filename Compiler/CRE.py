# encoding: utf-8
"""
CRE：编译运行时环境
编译运行时模块需要的变量，作为分担TRE功能和职责的产物，便于调试和拓展
此模块存放一些TVM运行时或编译文件时生成
且需要与其它文件共享的变量。
"""


# 程序编译时索引
run_index: dict = {"__main__": "FILE"}
# 当前编译模块，用于报错时锁定模块行号(通过程序运行时索引【run_index】)
CRE_module: str = "__main__"
# 异常链
# file_path: error
CRE_error_list: dict = {}
