# encoding: utf-8
"""
建立此模块是为了优化字节码以便于缩小文件体积和提升运行速度，
尽管在此处才正式表明进行优化，但是优化早在这之前开始
注：系统中尚未设置不使用编译优化的选项
"""


def optimize(codes: tuple) -> tuple:
    """优化字节码
    codes不仅为字节码，还应该包含常量池等多个部分"""
    ver = codes[0]
    codes_ = codes[1]
    constant_pool = codes[2]
    name_pool = codes[3]
    classes = codes[4]

    for line in range(len(codes_)):
        if codes_[line] == [(20, -1)]:
            codes_[line] = []

    return ver, codes_, constant_pool, name_pool, classes
