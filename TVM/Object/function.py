# encoding: utf-8
"""
函数相关的类
"""


class function:
    """函数"""

    def __init__(self, name, arguments):
        """
        运行时生成的类，负责调用，且有函数作用域
        @param name:函数名
        @param arguments:函数参数
        """
        self.name = name
        self.arguments = arguments
        # 局部变量
        self.environment = {}
        # 数据栈
        self.stack = []
        # TODO
