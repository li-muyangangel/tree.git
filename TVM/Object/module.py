"""
模块
"""
import sys

from .. import TRE

from loader.codes_loader import codes_dict
from Error.Error import send_error
import share


class Module:
    """
    本类只是一个数据的聚合体，记录着有关模块的运行时信息
    """

    def __init__(self, codes, path):
        # 数据栈
        self.stack = []
        # 模块
        self.module = {}
        # 函数
        self.mfunc = {}
        # 类
        self.mclass = {}
        # 文件全部内容
        self.file = codes
        # 版本号
        self.ver = self.file[0]
        # 指令集
        self.codes = []
        for i in self.file[1]:
            list_new = []
            for j in i:
                list_new.append((codes_dict[j[0]], j[1]))
            self.codes.append(list_new)
        # 值
        self.values = self.file[2]
        # 名字
        self.names = self.file[3]
        # 类型
        share.types.update(self.file[4])
        self.classes = share.types
        # 文件路径
        self.path = path
        # 调用自己的模块，在程序结束时切换到调用者
        self.parent = TRE.TRE_module
        self.name = path
        # 全局变量(内置一些变量)
        self.environment = {
            "__name__": (self.path, "string"),
            "__file__": (self.path, "srting"),
        }
        # 虚拟机拒绝解析比自己版本高的ctree文件
        if self.ver > share.ver:
            send_error("VersionError", self.ver, share.ver)
            sys.exit()
        TRE.run_index[self.path] = 0
