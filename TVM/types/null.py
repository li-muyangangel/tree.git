# encoding: utf-8
"""
null类型定义：空类型
"""


class null:
    """
    null类型，可以在数组初始化时占位
    """

    # 允许输出，输出null
    __value: str = "null"

    def __init__(self):
        pass

    def __str__(self):
        return self.__value

    def __repr__(self):
        return self.__value
