# encoding: utf-8
"""
数组类型定义
"""
from .null import null


class ArrayIndexError(Exception):
    """
    一个空的异常类，用于帮助检测异常
    """

    pass


class ArrayValueError(Exception):
    """
    一个空的异常类，用于帮助检测异常
    """

    pass


class array:
    """数组作为一个连续的储存类型，通过列表来实现，为Tree内置类型"""

    def __init__(self, long: int, data: list):
        """
        @param long: 数组长度
        @param data: 传入的初始化数据，当不够时，自动由null补齐
        """
        sub_long = long - len(data)
        self.__list = [null() for i in range(sub_long)]
        self.__list = list(data) + self.__list

    def __str__(self):
        return str(self.__list)

    def __repr__(self):
        return str(self.__list)

    def add(self, data):
        """
        向数组内添加元素
        @param data: 添加的数据类型
        """
        self.__list.append(data)

    def change(self, data, index):
        """
        依靠索引赋值
        @param data: 赋值的元素
        @param index: 索引
        @return: None
        """
        try:
            self.__list[index] = data
        except IndexError:
            raise ArrayIndexError()

    def pop(self, index=-1):
        """
        根据索引删除值
        @param index: 索引
        @return: 删除的元素
        """
        try:
            return self.__list.pop(index)
        except IndexError:
            raise ArrayIndexError()

    def remove(self, data):
        """
        删除元素（从开始的第一个元素查找直到查找到）
        @param data: 元素
        @return: 删除的元素
        """
        try:
            self.__list.remove(data)
        except ValueError:
            raise ArrayValueError()
        return data

    def len(self) -> int:
        """
        计算长度
        @return: int，长度
        """
        return len(self.__list)

    def count(self, data) -> int:
        """
        计算重复元素
        @return: int，长度
        """
        return self.__list.count(data)
