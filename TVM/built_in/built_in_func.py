# encoding: utf-8
"""
内置函数定义处
注意：内置函数的定义要符合以下要求：
1.有一个参数：参数
2.有返回值：类型不限
3.名字必须为全大写
"""

import sys
from Error.Error import send_error
from ..types.string import string


def LEN(value: list) -> int:
    """计算值的长度"""
    long = len(value)
    if long > 1 or long == 0:
        send_error("ArgumentError", '"len" needs only one argument')
    value = value[0]
    try:
        return len(value)
    except TypeError:
        send_error("ValueError", type(value).__name__, "len")


def HELP(value: list) -> None:
    """
    提供帮助：不接受参数
    这个函数只是为了帮助点明帮助文档的所在地，真正的帮助已经被我写成了文档
    """
    print(
        """
Welcome to Tree!If this is your first time using Tree,you can read the Doc to find help.
"""
    )


def PYTHON(value: list) -> None:
    """
    调用python代码
    """
    print(value)
    exec("\n".join(value))


TYPE = lambda value: [type(i).__name__ for i in value]


def INPUT(argv: list) -> str:
    long = len(argv)
    if long > 1:
        send_error(
            "ArgumentError",
            f"input only needs one argumnet.But there are {arguments_num} arguments",
        )
    elif long != 0:
        print(argv[0], end="")
    return input()


def PRINT(argv: list) -> None:
    """不换行打印栈顶的值"""
    for i in argv:
        print(i, end="")


def PRINTLN(argv: list) -> None:
    """换行打印栈顶的值"""
    for i in argv:
        print(i, end="")
    print("\n", end="")


def INT(argv):
    """取出变量的数据并改变为int后压入栈中"""
    try:
        return int(argv[0])
    except ValueError:
        send_error("ValueError", str(value), "int")
    except IndexError:
        send_error("ArgumentError", "int needs one argument")


def STRING(argv):
    """取出变量的数据并改变为string后压入栈中"""
    try:
        return string(argv[0])
    except ValueError:
        send_error("ValueError", value, "string")
    except IndexError:
        send_error("ArgumentError", "string needs one argument")


def FLOAT(argv):
    """取出变量的数据并改变为float后压入栈中"""
    try:
        return float(argv[0])
    except ValueError:
        send_error("ValueError", str(value), "float")
    except IndexError:
        send_error("ArgumentError", "float needs one argument")


def EXIT(argv):
    """退出当前线程运行"""
    sys.exit()
