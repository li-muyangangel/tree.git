# encoding: utf-8
"""
此模块存放一些TVM运行时或编译文件时生成
且需要与其它文件共享的变量。
"""


# 程序运行时索引，设为file是因为防止不是在运行时出错时报错
run_index: dict = {"__main__": "FILE"}
# 当前运行模块，用于报错时锁定模块行号(通过程序运行时索引【run_index】)
TRE_module: str = "__main__"
# 异常链
# {file_path: error}
TRE_error_list: dict = {}
