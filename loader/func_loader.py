# encoding: utf-8
"""
本模块负责储存内置函数与编码的对应的一些常量
"""


# 负责储存时将函数名替换为整数，减少ctree文件大小
num_func = {
    0: "len",
    1: "help",
    2: "python",
    3: "type",
    4: "print",
    5: "println",
    6: "input",
    7: "string",
    8: "int",
    9: "float",
    10: "exit",
}

# 逆转键值对的值，负责解析文件或运行文件
func_num = dict(zip(num_func.values(), num_func.keys()))

builtin_func_name: list = list(num_func.values())
