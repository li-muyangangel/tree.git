# encoding: utf-8
"""
类文件的储存方式，配合class_loader的解析
"""


def build_class(name: str, class_data: list, class_method: list) -> list:
    """
    接受一些数据，将其转化为class
    @param name: 类名
    @param class_data: 类的属性（不包括方法）
    @param class_method: 类的方法
    @return: 列表储存的类结构
    """
    # 最后一个参数留给方法，第一个列表为公有方法，第二个为私有方法
    result = [name, class_data, [[], []]]
    for i in class_method:
        if len(i) <= 3:
            # 长度不足以容下私有方法，为公有方法
            result[2][0].append(i)
        elif i[:2] != "__":
            # 长度足够但不以__开头，为公有方法
            result[2][0].append(i)
        else:
            # 私有方法
            result[2][1].append(i)
    return result
