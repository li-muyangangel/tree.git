# encoding: utf-8
"""
本模块负责储存指令与整数的对应的一些常量
"""


# 负责储存时将助记符替换为整数，减少ctree文件大小
codes_dict = {
    0: "ADD",
    1: "SUB",
    2: "MUL",
    3: "DIV",
    4: "POW",
    5: "MOD",
    6: "LOAD_VALUE",
    7: "STORE_NAME",
    8: "LOAD_NAME",
    9: "CHANGE_VALUE",
    10: "POP_VALUE",
    11: "IMPORT",
    12: "JUMP_IF_FALSE",
    13: "GOTO",
    14: "EQUAL",
    15: "UNEQUAL",
    16: "GREATER",
    17: "LESS",
    18: "LESS_EQUAL",
    19: "GREATER_EQUAL",
    20: "NOP",
    21: "DUP",
    22: "SWAP",
    23: "FUNC",
    24: "RUN_FUNC",
    25: "LEN_VALUE",
    26: "RETURN_VALUE",
    27: "ARRAY",
    28: "ARRAY_VALUE",
    29: "DEL",
    30: "ASSERT",
    31: "BUILTIN_FUNC",
    32: "CLASS",
    33: "ZDIV",
}


# 逆转键值对的值，负责解析文件或运行文件
f_codes = dict(zip(codes_dict.values(), codes_dict.keys()))
