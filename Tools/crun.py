# encoding: utf-8
"""
tree不生成中间文件的选项：
但它依旧是通过与原本相同的步骤运行的，
只不过它的字节码保存在内存中而不是文件里
"""
import sys
import os
from TVM.TVM import Interpreter
from Compiler.Compiler import Compiler
from Error.Error import send_error


def crun(path: str) -> None:
    """编译，运行，不生成编译文件"""
    try:
        with open(path, "r", encoding="UTF-8") as f:
            codes = Compiler(f.read())
            run_path = os.path.split(path)[0]
            run_name = os.path.splitext(os.path.split(path)[1])[0] + ".ctree"
            run_finally = os.path.join(run_path, run_name)
            interpreter = Interpreter(codes, run_finally)
            interpreter.run_code()
    except (FileNotFoundError, UnicodeDecodeError):
        send_error("OpenFileError", path)


if __name__ == "__main__":
    file_list = sys.argv[1:]
    for i in file_list:
        crun(i)
