# encoding: utf-8
"""
为了让系统更加容易更新，特意提供了这个制作更新包的工具，按照固定的格式生成更新包
注意：本工具对python脚本及exe文件皆有效果,前提是被更新系统与之相对应
"""

import sys
import os
import shelve
import shutil
from Error.Error import send_error


def buildinstall(argvlist: list) -> None:
    """按照一定程序，生成一个文件夹
    dirpath：生成文件夹的地址
    file_dir:需要打包的文件或文件夹
    """
    try:
        dirpath: str = argvlist[0]
        file_dir: list = argvlist[1:]
    except IndexError:
        print("Error:args are error.")
        sys.exit()
    dirpath = os.path.abspath(dirpath)
    dirpath = os.path.join(dirpath, "new")
    try:
        os.makedirs(dirpath)
    except FileExistsError:
        pass
    min_ver = input("Smallest_version:")
    max_ver = input("Biggest_version:")
    with shelve.open(os.path.join(dirpath, "update"), writeback=True) as file:
        file["biggest_ver"] = max_ver
        file["smallest_ver"] = min_ver
        file["update"] = True
        file["files"] = []
        for i in file_dir:
            file["files"].append(i)

    for i in file_dir:
        new_file_path = os.path.join(dirpath, os.path.split(i)[1])
        if os.path.isfile(i):
            shutil.copyfile(i, new_file_path)
        elif os.path.isdir(i):
            shutil.copytree(i, new_file_path)
        else:
            print(f"Error:couldn't find {i}")
    print("Setup was made.")


if __name__ == "__main__":
    buildinstall(sys.argv[1:])
