# encoding: utf-8
"""
一个运行功能：运行目录下所有ctree文件
"""
import sys
import os
from Error.Error import send_error
from .run import run


def run_all(path: str) -> None:
    try:
        for root, dirs, files in os.walk(path):
            root = os.path.abspath(root)
            for i in files:
                cpath = os.path.join(root, i)
                if ".ctree" == os.path.splitext(i)[1]:
                    run(cpath)
    except FileNotFoundError:
        send_error("OpenFileError", path)


if __name__ == "__main__":
    file_list = sys.argv[1:]
    for i in file_list:
        run_all(i)
