# encoding: utf-8
"""
一个调试功能：输出tree划分的词流
"""
import sys
from TVM.TVM import Interpreter
from Compiler.Compiler import start
from Compiler.token import token
from Error.Error import send_error


def Token(path: str) -> None:
    """编译为词法单元并输出（不保存）"""
    print(f"From '{path}':")
    try:
        with open(path, encoding="UTF-8") as file:
            result = start(file.read())
            result = token(result)
            for i in range(len(result)):
                print(i, ":", result[i])
            print("\n")
    except FileNotFoundError:
        send_error("OpenFileError", path)


if __name__ == "__main__":
    file_list = sys.argv[1:]
    for i in file_list:
        Token(i)
