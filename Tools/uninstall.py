"""
Tree的内置卸载程序，其实只需把安装Tree的文件夹删除即可，本卸载程序基本用不上
"""
import os
import shutil


def uninstall():
    unin = input("uninstall: Do you want to uninstall Tree?(y/n)")
    if unin == "y":
        uninstall_path = os.path.dirname(os.path.dirname(__file__))
        shutil.rmtree(uninstall_path)
        input("uninstall:Tree was uninstalled.")
    elif unin == "n":
        input("uninstall:exit.")
    else:
        print(f'Error:uninstall didn\'t have instruction "{unin}"')


if __name__ == "__main__":
    uninstall()
