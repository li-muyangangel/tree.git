# encoding: utf-8
"""
关于tree虚拟机运行，执行程序：从本地读取并传递给虚拟机
"""
import pickle
import sys
from TVM.TVM import Interpreter
from Error.Error import send_error


def run(path: str) -> None:
    """执行指令集"""
    try:
        file = open(path, "rb")
    except FileNotFoundError:
        send_error("OpenFileError", path)

    runcode = file.read()
    try:
        interpreterrun = Interpreter(pickle.loads(runcode), path)
        interpreterrun.run_code()
    except EOFError:
        # 文件为空
        print(f"File {path} is empty.")
    except Exception:
        # ctree文件为假
        print(f"file {path} is not a ctree file.")

    file.close()


if __name__ == "__main__":
    file_list = sys.argv[1:]
    for i in file_list:
        run(i)
