# encoding: utf-8
"""
清理目录模块中的ctree文件
"""
import os
import sys
from Error.Error import send_error


remove_count = 0


def clean(path):
    """清除目录下所有ctree文件"""
    global remove_count
    try:
        for root, dirs, files in os.walk(path):
            root = os.path.abspath(root)
            for i in files:
                cpath = os.path.join(root, i)
                if ".ctree" == os.path.splitext(i)[1]:
                    os.remove(cpath)
                    remove_count += 1
                    print(f"Remove {cpath}")
            print(f"Remove {remove_count} files from {path}\n\n")
    except FileNotFoundError:
        send_error("OpenFileError", i)


def count_remove(path, func=clean):
    global remove_count
    remove_count = 0
    func(path)
    remove_count = 0


if __name__ == "__main__":
    file_list = sys.argv[1:]
    for i in file_list:
        try:
            count_remove(i)
        except FileNotFoundError:
            send_error("OpenFileError", i)
