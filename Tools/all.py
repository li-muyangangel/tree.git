# encoding: utf-8
"""
一个编译的延伸功能：编译目录下所有ctree文件
"""
import sys
import os
from Error.Error import send_error
from .build import build


# 统计编译数量的变量
__count = 0


def compile_all(path: str) -> None:
    global __count
    try:
        for root, dirs, files in os.walk(path):
            root = os.path.abspath(root)
            for i in files:
                cpath = os.path.join(root, i)
                if ".tree" == os.path.splitext(i)[1]:
                    build(cpath)
                    __count += 1
                    print(f"Build {cpath}")
            print(f"Build {__count} files now.Compiled from {root}\n\n")
    except FileNotFoundError:
        send_error("OpenFileError", cpath)


def count_build(path: str, func=compile_all) -> None:
    print("Tree start to build.\n")
    global __count
    __count = 0
    func(path)
    __count = 0
    print("Finished.")


if __name__ == "__main__":
    file_list = sys.argv[1:]
    for i in file_list:
        count_build(i)
