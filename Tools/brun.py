# encoding: utf-8
"""
编译工具:编译并执行
"""
import sys
import os
from .build import build
from .run import run


def brun(path: str) -> None:
    """编译并执行"""
    build(path)
    run_path = os.path.split(path)[0]
    run_name = os.path.splitext(os.path.split(path)[1])[0] + ".ctree"
    run_finally = os.path.join(run_path, run_name)
    run(run_finally)


if __name__ == "__main__":
    file_list = sys.argv[1:]
    for i in file_list:
        brun(i)
